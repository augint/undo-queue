const util = require('./util');

module.exports = class queueItem {
  constructor( table, whereField, whereValue ) {
    this.id = util.guid();
    this.table = table;
    this.whereField = whereField;
    this.whereValue = whereValue;
    this.timer = setInterval( () => { this.timerState = true; clearInterval(this.timer); }, 3000);
    this.timerState = false;
    this.state = 1; // 1: waiting | 2: processing | 3: timeout error | 4: invalid error
  }
};