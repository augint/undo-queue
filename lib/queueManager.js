const QueueItem = require('./queueItem'),
      DBManager = require('./dbManager'),
           util = require('./util');

module.exports = class QueueManager {

  constructor( dbManager ) {
    this.q = [];
    this.timer = undefined;
    if ( dbManager instanceof DBManager )
      this.dbManager = dbManager;
    else throw new Error( 'QueueManager requires a valid DBManager.' );
  }

  get len() { return this.q.length; }

  qPurge() {
    if ( this.len ) {
      this.q.forEach( (v,i,a) => {
        if ( !( v instanceof QueueItem ) || v.state instanceof util.EInvalid ){
          this.q = this.q.slice( 0, i ).concat( i < this.len - 1 ? this.q.slice( i + 1 ) : [] );
          a = a.slice( 0, i ).concat( i < this.len - 1 ? a.slice( i + 1 ) : [] );
          console.log(`[TEST] Queue Size: ${this.len}`);
        }
      });
    }
  }

  qAdd( obj ) {
    if ( !obj.hasOwnProperty('table') || !obj.hasOwnProperty('field') || !obj.hasOwnProperty('value') ) {
      console.log('[INFO] QueueManager.qAdd failure: Item not added - missing required properties.');
    } else {
      this.q.push( new QueueItem( obj.table, obj.field, obj.value ) );
      if ( this.timer === undefined )
      this.timer = setInterval( () => { this.qProcess(); }, 3000);
      console.log(`[TEST] Queue Size: ${this.len}`);
      console.log('[INFO] QueueManager.qAdd success: Item added to queue.');
    }
  }

  qRemove( guid ) {
    this.qPurge(); // Purge failure items & those not of QueueItem Prototype
    // Ensure there are items in the queue
    if ( this.len ) {
      // If guid argument is passed, remove specific item.
      if ( guid ) {
        if ( !this.q.some( ( v, i ) => { if ( v.id === guid ) {
              this.q = this.q.slice( 0, i ).concat( i < this.len - 1 ? this.q.slice( i + 1 ) : [] );
              console.log(`[TEST] Queue Size: ${this.len}`);
              console.log( '[INFO] QueueManager.qRemove success: Queue item with ID "' + guid + '" removed.' );
              return true;}}) ) console.log( '[INFO] QueueManager.qRemove failure: Queue item with ID "' + guid + '" does not exist!' );

      } else {
        // Check that last item is within 'undo' period
        if ( !this.q[ this.len - 1 ].timerState ) {
          this.q.pop(); // If so, remove it
          // ... logging
          console.log(`[TEST] Queue Size: ${this.len}`);
          console.log( '[INFO] QueueManager.qRemove success: Queue item removed via undo.' );
        } else console.log( '[INFO] QueueManager.qRemove failure: Queue item timeout passed.' );
      }
    } else console.log( '[INFO] QueueManager.qRemove failure: No items in queue!' );
  }

  qProcess( list ) {
    console.log( 'qProcess called!' );
    let toProcess = Array.isArray(list) ? list : [];
    this.qPurge(); // Purge failure items & those not of QueueItem Prototype
    // Ensure there are items remaining in the queue.
    if ( this.len ) {
      if ( toProcess.length < 1 ) this.q.forEach( ( v, i, a ) => {
        // Retry timed-out requests & those whose timer is finished.
        if ( ( v.timerState && v.state === 1 ) || v.state instanceof util.ETimeout ) {
          v.state = 2;
          toProcess.push( v );
        }
      });

      if ( toProcess.length ) {
        this.dbManager.process( toProcess, ( err, res ) => {
          if ( err ) {
            if ( err instanceof util.ETimeout ) {
              console.log( '[INFO] QueueManager.qProcess: Request timed out, trying again momentarily.' );
              toProcess.forEach( v => { this.q.some( i => { if ( i.id === v.id ) { i.state = 1; return true; }}); });
            } else if ( err instanceof util.EInvalid ) {
              if ( toProcess.length > 1 ) {
                console.log( '[INFO] QueueManager.qProcess: Request was invalid. Trying again with single statement.' );
                toProcess.slice(1).forEach( v => {
                  this.q.some( i => {
                    if ( i.id === v.id ) {
                      i.state = 1;
                      return true;
                    }
                  });
                });
                clearInterval(this.timer);
                let i2 = setInterval(()=> {
                  this.qProcess( [ toProcess[0] ] );
                  clearInterval(i2);
                  this.timer = setInterval( () => { this.qProcess(); }, 3000);
                }, 1000 );
              } else {
                toProcess.forEach( v => {
                  this.q.some( i => {
                    if ( i.id === v.id ) {
                      this.qRemove( i.id );
                      console.log(`[TEST] Queue Size: ${this.len}`);
                      console.log( '[INFO] QueueManager.qProcess: Request was invalid. Removed item with id "' + i.id + '".' );
                      return true;
                    }
                  });
                });
              }
            } else throw( err );
          } else {
            // If res is true, dbManager was busy.
            if ( res ) {
              console.log( '[INFO] QueueManager.qProcess: dbManager busy. Retrying.' );
              this.qProcess( toProcess );
            }
            else {
              toProcess.forEach( v => {
                this.q.some( i => {
                  if ( i.id === v.id ) {
                    this.qRemove( i.id );
                    console.log(`[TEST] Queue Size: ${this.len}`);
                    console.log( '[INFO] QueueManager.qProcess: Success. Removed item with id "' + i.id + '" from database.' );
                    return true;
                  }
                });
              });
            }
          }
        });
      }
    } else {
      clearInterval( this.timer );
      this.timer = undefined;
    }
  }

};