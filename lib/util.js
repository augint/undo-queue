(function() {

  let exports = {};

  exports.guid = guid;
  function guid() {
    function s4() {
      return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
    }
    return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
  }

  class EInvalid extends Error {}
  exports.EInvalid = EInvalid;

  class ETimeout extends Error {}
  exports.ETimeout = ETimeout;

  module.exports = exports;

}).call(this);