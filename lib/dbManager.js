const util = require('./util');
module.exports = class DBManager {

  constructor( dbdetails ) {
    this.dbdetails = dbdetails; // This won't be used - just an example.
    this.busy = false;
  }

  process( deletions, callback ) {
    // One request at a time.
    if ( this.busy ) { callback( null, true ); }
    else {

      let table = deletions[0].table;
      // If all the requests are to the same table...
      if ( deletions.every( v => v.table === table ) ) {
        // Where x=y OR a=b OR ...etc
        let conditions = deletions.map( v => v.whereField + '=' + ( typeof v.whereValue === 'number' ? v.whereValue : '"' + v.whereValue + '"' ) );
        let statement = `DELETE FROM ${table} WHERE ${ conditions.join( ' OR ' ) }`;

        this.busy = true;
        // execute statement on db... get success/failure return
        // do something with 'statement'
        this.busy = false;

        // ...going to simulate random pass/fails
        if ( ( Math.random() * 100 ) < 50 ) {
          // Fail
          if ( ( Math.random() * 100 ) < 50 ) {
            // Timeout
            callback( new util.ETimeout() );
            // Invalid
          } else callback( new util.EInvalid() );
        } else callback( null );
      }
    }
  }
};