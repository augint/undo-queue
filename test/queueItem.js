const QM = require( '../lib/queueManager' );
const DB = require( '../lib/dbManager' );

// Example DB Credentials
let qm, dm = new DB( { db: 'somedb', user: 'someuser', pass: 'somepass' });

try { qm = new QM( dm ); }
catch(e) { console.log(e); }

// SIMULATE USER INPUT
let i1 = setInterval( () => {
  qm.qRemove();
  clearInterval(i1);
}, 1000 );

let i2 = setInterval( () => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i2);
}, 2000 );

let i21 = setInterval( () => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i21);
}, 2100 );

let i3 = setInterval(() => {
  qm.qRemove();
  clearInterval(i3);
}, 3000 );

let i31 = setInterval( () => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i31);
}, 3100 );

let i34 = setInterval( () => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i34);
}, 3400 );

let i4 = setInterval(() => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i4);
}, 4000 );

let i5 = setInterval(() => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i5);
}, 5000 );

let i6 = setInterval(() => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i6);
}, 6000 );

let i7 = setInterval(() => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i7);
}, 7000 );

let i8 = setInterval(() => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i8);
}, 8000 );

let i9 = setInterval(() => {
  qm.qRemove();
  clearInterval(i9);
}, 9000 );

let i10 = setInterval(() => {
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  qm.qAdd({ table: 'tableName', field: 'ID', value: 1 });
  clearInterval(i10);
}, 22000);